<?php

/**
 * Here we add our routes.
 */

use App\Core\Router\Router;

$router = new Router();

$router->get('/', 'HomeController@index', true);

$router->get('/login', 'UserController@login');
$router->post('/login', 'UserController@postLogin');

$router->get('/logout', 'UserController@logout', true);

$router->get('/register', 'UserController@register');
$router->post('/register', 'UserController@postRegister');

$router->get('/create-team', 'TeamController@createTeam');
$router->post('/create-team', 'TeamController@postCreateTeam');
$router->get('/delete-team', 'TeamController@deleteTeam');

$router->get('/create-player', 'PlayerController@createPlayer');
$router->post('/create-player', 'PlayerController@postCreatePlayer');
$router->get('/delete-player', 'PlayerController@deletePlayer');

$router->get('/simulate-match-1', 'MatchController@simulateMatchOne');
$router->get('/simulate-match-2', 'MatchController@simulateMatchTwo');
$router->get('/simulate-match-3', 'MatchController@simulateMatchThree');

$router->post('/complete-match', 'MatchController@completeMatch');
<?php

namespace App\Controllers;

use App\Models\Player, App\Core\Database\DBFacade as DB;

class MatchController
{
    /**
     * Game 1 type - described inside
     * @return view
     */
    public function simulateMatchOne()
    {
        /*
         * You are playing against stronger opponent, so you need to concentrate on defence.
         * You should choose 5 best defenders, 4 best midfielders and the fastest striker for counter attacks.
         */

        $rules = [
            'maxGoalies' => 1,
            'maxDefenders' => 5,
            'maxMidfielders' => 4,
            'maxStrikers' => 1
        ];

        $sqlQueryArray = [
            "(select * from players where position like 'Goalie' and injured = false order by quality desc, speed desc  limit {$rules['maxGoalies']})",
            "(select * from players where position like 'Defender' and injured = false order by quality desc, speed desc limit {$rules['maxDefenders']})",
            "(select * from players where position like 'Midfielder' and injured = false order by quality desc, speed desc limit {$rules['maxMidfielders']})",
            "(select * from players where position like 'Striker' and injured = false order by speed desc, quality desc limit {$rules['maxStrikers']})"
        ];

        return $this->_renderMatchSimulationView($sqlQueryArray);
    }

    /**
     * Game 2 type - described inside
     * @return view
     */
    public function simulateMatchTwo()
    {
        /*
         * Second opponent seams equal to yours team quality. Build a regular, 4-4-2 formation of best players.
         */

        $rules = [
            'maxGoalies' => 1,
            'maxDefenders' => 4,
            'maxMidfielders' => 4,
            'maxStrikers' => 2
        ];

        /*
         * if by "best" players means combination of two params (quality and speed) then order would be somethin like this:
         * ORDER BY ((quality+speed)/2) DESC
         * or maybe we can add some importance constant like this:
         * ORDER BY (((quality*0.6)+(speed*0.4))/2) DESC
         */
        $sqlQueryArray = [
            "(select * from players where position like 'Goalie' and injured = false order by quality desc, speed desc  limit {$rules['maxGoalies']})",
            "(select * from players where position like 'Defender' and injured = false order by quality desc, speed desc limit {$rules['maxDefenders']})",
            "(select * from players where position like 'Midfielder' and injured = false order by quality desc, speed desc limit {$rules['maxMidfielders']})",
            "(select * from players where position like 'Striker' and injured = false order by quality desc, speed desc limit {$rules['maxStrikers']})"
        ];

        return $this->_renderMatchSimulationView($sqlQueryArray);
    }

    /**
     * Game 3 type - described inside
     * @return view
     */
    public function simulateMatchThree()
    {
        /*
         * The weakest opponent and we need 3 points to qualify into the finals.
         * Lets attack! Attacking formation is 3-4-3.
         * Focus on players quality as counter attacks are not important in this game.
         */

        $rules = [
            'maxGoalies' => 1,
            'maxDefenders' => 3,
            'maxMidfielders' => 4,
            'maxStrikers' => 3
        ];

        $sqlQueryArray = [
            "(select * from players where position like 'Goalie' and injured = false order by quality desc, speed desc  limit {$rules['maxGoalies']})",
            "(select * from players where position like 'Defender' and injured = false order by quality desc, speed desc limit {$rules['maxDefenders']})",
            "(select * from players where position like 'Midfielder' and injured = false order by quality desc, speed desc limit {$rules['maxMidfielders']})",
            "(select * from players where position like 'Striker' and injured = false order by quality desc, speed desc limit {$rules['maxStrikers']})"
        ];

        return $this->_renderMatchSimulationView($sqlQueryArray);
    }

    public function completeMatch()
    {
        $ids = input('ids');

        if (empty($ids)) {
            $returnBack = $_SERVER['HTTP_REFERER'];
            redirect($returnBack, ['error' => 'Missing ids for injury.']);
        }

        $ids = explode(',', $ids);
        $injury = '';

        if (count($ids) >= 11) {
            $randomPlayerForInjury = array_rand(array_flip($ids));

            $player = Player::find($randomPlayerForInjury);
            $player->injured = true;
            $player->save();

            $injury .= " Player {$player->name} injured in last game.";
        }

        redirect('', ['success' => "Successfully finished the match.{$injury}"]);
    }

    /**
     * Format players array, grouping them into position
     * @param $players
     * @return array
     */
    private function _formatPlayers($players)
    {
        $formatedPlayers = [];

        foreach ($players as $player) {
            $formatedPlayers['players'][$player->position][] = $player;
        }

        $formatedPlayers['team'] = $players[0]->team();

        return $formatedPlayers;
    }

    /**
     * Render match simulation view depending of player number
     * @param $sqlQueryArray
     * @return view
     */
    private function _renderMatchSimulationView($sqlQueryArray)
    {
        $sqlQuery = implode(' union ', $sqlQueryArray);

        $players = DB::selectRaw($sqlQuery, Player::class);

        if (count($players) < 11) {
            $playerIds = [];
            foreach ($players as $player) {
                $playerIds[] = $player->id;
            }
            $playerIds = implode(',', $playerIds);
            return view('match_simulation_auto_loss', compact('playerIds'));
        }

        $players = $this->_formatPlayers($players);

        return view('match_simulation', compact('players'));
    }
}
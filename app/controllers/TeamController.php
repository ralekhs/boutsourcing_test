<?php

namespace App\Controllers;

use App\Models\Team, App\Validators\Validator;

class TeamController
{
    private $validator;

    public function __construct()
    {
        $this->validator = Validator::instance();
    }

    /**
     * create team view
     */
    public function createTeam()
    {
        if (!empty(auth()->team())) {
            return redirect('', ['error' => 'You already have a team.']);
        }
        return view('create_team');
    }

    /**
     * create team handler
     */
    public function postCreateTeam()
    {
        $data = input();

        // validate input
        $errors = $this->validator->validateTeam($data);
        if (count($errors)) {
            return view('create_team', ['errors' => $errors, 'old_input' => $data]);
        }

        // create new Team
        $team = new Team();
        $team->name = $data['name'];
        $team->coach_id = auth()->id;

        // check if team creation went well and redirect to home page
        if ($team->save()) {
            return redirect('', ['success' => 'Successfully created team.']);
        }
        return view('create_team', ['errors' => ['Something went wrong with team creation.'], 'old_input' => $data]);
    }

    /**
     * Delete team of logged in coach
     */
    public function deleteTeam()
    {
        if (auth()->team()->delete()) {
            return redirect('', ['success' => 'You have successfully deleted your team.']);
        }

        return redirect('', ['error' => 'Something went wront with deleting team.']);
    }
}
<?php

namespace App\Controllers;

use App\Models\User, App\Validators\Validator;

class UserController
{
    private $validator;

    public function __construct()
    {
        $this->validator = Validator::instance();
    }

    /**
     * Login page render
     */
    public function login()
    {
        return view('login');
    }

    /**
     * Login handler
     */
    public function postLogin()
    {
        $data = input();

        // input validation
        $errors = $this->validator->validateLogin($data);
        if (count($errors)) {
            return view('login', compact('errors'));
        }

        // find user under given condition
        $condition = [
            [
                'email',
                '=',
                $data['email']
            ]
        ];

        $user = User::find($condition);

        // check if user exists
        if ($user) {
            // check password
            if (password_verify($data['password'], $user->password)) {
                // login user and redirect to home page
                userLogin($user);
                return redirect('', ['success' => 'You have successfully logged in.']);
            }
            return view('login', ['errors' => [
                'Wrong credentials'
            ]]);
        }
        return view('login', ['error' => 'Wrong credentials.']);
    }

    /**
     * Logout handler
     */
    public function logout()
    {
        userLogout();
        return redirect('');
    }

    /**
     * Register page render
     */
    public function register()
    {
        return view('register');
    }

    /**
     * Register handler
     */
    public function postRegister()
    {
        $data = input();

        // validate input
        $errors = $this->validator->validateRegistration($data);
        if (count($errors)) {
            return view('register', ['errors' => $errors, 'old_input' => $data]);
        }

        // unset repeated pasword, we dont need it in register action
        unset($data['password_repeat']);

        // hashing password with bcrypt
        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT, [
            'cost' => 14
        ]);

        // create new User
        $user = new User();
        $user->email = $data['email'];
        $user->password = $data['password'];

        // check if registration went well and redirect to home page
        if ($user->save()) {
            return redirect('', ['success' => 'Successfully registered.']);
        }
        return view('register', ['errors' => ['Something went wrong with registration.'], 'old_input' => $data]);
    }
}
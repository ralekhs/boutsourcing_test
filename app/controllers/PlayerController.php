<?php

namespace App\Controllers;

use App\Models\Player, App\Validators\Validator;

class PlayerController
{
    private $validator;

    public function __construct()
    {
        $this->validator = Validator::instance();
    }

    /**
     * create player view
     */
    public function createPlayer()
    {
        $positions = Player::positions;

        if (count(auth()->team()->players()) == 22) {
            return redirect('', ['error' => 'You can\'t have more than 22 players.']);
        }

        return view('create_player', compact('positions'));
    }

    /**
     * handle create player
     */
    public function postCreatePlayer()
    {
        $data = input();

        // validate input
        $errors = $this->validator->validatePlayer($data);
        if (count($errors)) {
            return view('create_player', ['errors' => $errors, 'old_input' => $data]);
        }

        // create new Player
        $player = new Player();
        $player->name = $data['name'];
        $player->position = $data['position'];
        $player->quality = $data['quality'];
        $player->speed = $data['speed'];

        $player->team_id = auth()->team()->id;

        // check if player creation went well and redirect to home page
        if ($player->save()) {
            return redirect('', ['success' => 'You have successfully registered a player into your team.']);
        }

        return redirect('create-player', ['error' => 'Something went wrong with player creation.', 'old_input' => $data]);
    }

    /**
     * handle player delete
     */
    public function deletePlayer()
    {
        $id = input('id');
        if (empty($id)) {
            return redirect('', ['error' => 'Please provide ID of a player that you want do delete.']);
        }

        // check if delete player went well and redirect to home page
        if (Player::find($id)->delete()) {
            return redirect('', ['success' => 'You have successfully deleted a player.']);
        }

        return redirect('', ['error' => 'Something went wrong with deleting player.']);
    }
}
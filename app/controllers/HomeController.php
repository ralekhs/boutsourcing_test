<?php

namespace App\Controllers;

class HomeController
{
    /**
     * Home page render
     */
    public function index()
    {
        $team = auth()->team();
        return view('home', compact('team'));
    }

}

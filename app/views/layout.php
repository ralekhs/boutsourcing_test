<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="author" content="Ratko Buncic">
    <title>Boutsourcing Test - Ratko Buncic</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo publicUrl('css/style.css') ?>">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 top-header">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <h1>
                        Boutsourcing Test - Ratko Buncic
                    </h1>
                </div>
            </div>
        </div>
        <div class="col-md-offset-3 col-md-6 padding-top padding-bottom">
            <div class="row">
                <div class="col-md-3">
                    <ul class="nav nav-pills nav-stacked">
                        <?php
                        if (checkAuth()) {
                            ?>
                            <li <?php echo getData('view') == 'home' ? 'class="active"' : '' ?>>
                                <a href="<?php echo publicUrl() ?>">Home</a>
                            </li>
                            <?php
                        }
                        ?>
                        <li <?php echo getData('view') == 'login' ? 'class="active"' : '' ?>>
                            <?php
                            if (checkAuth()) {
                                ?>
                                <a href="<?php echo publicUrl('logout') ?>">Logout</a>
                                <?php
                            } else {
                                ?>
                                <a href="<?php echo publicUrl('login') ?>">Login</a>
                                <?php
                            }
                            ?>
                        </li>
                        <?php
                        if (!checkAuth()) {
                            ?>
                            <li <?php echo getData('view') == 'register' ? 'class="active"' : '' ?>>
                                <a href="<?php echo publicUrl('register') ?>">Register</a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <?php
                    if (getData('name')) {
                        ?>
                        <div class="alert alert-success user-signin">
                            <i>Welcome <b><?php echo getData('name') ?></b>!</i>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-md-9">
                    <?php
                    // Here we render particular views
                    getView(getData('view'));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script type="application/javascript">
    $(function(){
        $(document).on('click', '.prompt-player-delete', function(e){
            if (!confirm('Are you sure you wanna delete that player?')) {
                e.preventDefault();
            }
        });
        $(document).on('click', '.prompt-team-delete', function(e){
            if (!confirm('Are you sure you wanna delete your team?')) {
                e.preventDefault();
            }
        });
    });
</script>
</body>
</html>
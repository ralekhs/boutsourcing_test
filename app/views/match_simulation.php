<?php
$players = getData('players');

// display flash messages
if (getData('success')) {
    echo printSuccess(getData('success'));
}
if (getData('error')) {
    echo printErrors(getData('error'));
}
?>
<h1 class="home-title">Roster for the match</h1>
<?php
$fontSize = 12;
$playerIdsForInjury = [];
foreach ($players['players'] as $typeName => $type) {
    ?>
    <div class="align-center">
        <div class="col-md-9">
            <div class="row">
                <table class="table">
                    <tr>
                        <?php
                        foreach ($type as $player) {
                            ?>
                            <td style="width:<?php echo 100 / count($type) ?>%;text-align:center;font-style:italic;font-size:<?php echo $fontSize ?>px">
                                <?php
                                $playerIdsForInjury[] = $player->id;
                                echo $player->name;
                                ?>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-3">
            <div class="row text-center">
                <?php
                echo $typeName . 's line'
                ?>
            </div>
        </div>
    </div>
    <?php
    $fontSize += 4;
}
?>
<div>
    <form action="<?php echo publicUrl('complete-match')?>" method="post">
        <input type="hidden" name="ids" value="<?php echo implode(',', $playerIdsForInjury)?>">
        <button class="btn btn-primary" type="submit">Complete match</button>
    </form>
</div>

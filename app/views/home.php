<?php
$team = getData('team');
// display flash messages
if (getData('success')) {
    echo printSuccess(getData('success'));
}
if (getData('error')) {
    echo printErrors(getData('error'));
}
?>
    <h1 class="home-title">Hey coach, welcome!</h1>
<?php
if ($team) {
    $teamPlayers = $team->players();
    if (!empty($teamPlayers)) {
        ?>
        <div class="padding-top">
            <?php
            if (count($teamPlayers) == 22) {
                ?>
                <a class="btn btn-primary" href="<?php echo publicUrl('simulate-match-1') ?>">Simulate match type 1</a>
                <a class="btn btn-primary" href="<?php echo publicUrl('simulate-match-2') ?>">Simulate match type 2</a>
                <a class="btn btn-primary" href="<?php echo publicUrl('simulate-match-3') ?>">Simulate match type 3</a>
                <?php
            } else {
                ?>
                <div class="alert alert-info">
                    In order to simulate matches you must have exactly 22 players.
                </div>
                <?php
            }
            ?>
        </div>
        <h2>
            Your roster:
        </h2>
        <table class="table">
            <thead>
            <tr>
                <th>Position</th>
                <th>Name</th>
                <th>Quality</th>
                <th>Speed</th>
                <th>Injured</th>
            </tr>
            </thead>
            <?php
            foreach ($teamPlayers as $player) {
                ?>
                <tr>
                    <?php
                    switch ($player->position) {
                        case 'Goalie':
                            echo "<td><span class='label label-warning'>{$player->position}</span></td>";
                            break;
                        case 'Defender':
                            echo "<td><span class='label label-info'>{$player->position}</span></td>";
                            break;
                        case 'Midfielder':
                            echo "<td><span class='label label-success'>{$player->position}</span></td>";
                            break;
                        case 'Striker':
                            echo "<td><span class='label label-danger'>{$player->position}</span></td>";
                            break;
                        default:
                            echo "<td><span class='label label-warning'>{$player->position}</span></td>";
                    }
                    echo "<td>{$player->name}</td>
                            <td>{$player->quality}</td>
                            <td>{$player->speed}</td>
                            <td>" . ($player->injured ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '') . "</td>
                            <td>
                                <a href='" . publicUrl('delete-player?id=' . $player->id) . "' class='prompt-player-delete'>
                                    <span class='glyphicon glyphicon-remove text-danger'></span>
                                </a>
                            </td>";
                    ?>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <div class="pull-right">
            <?php
            if (is_array($teamPlayers) && count($teamPlayers) != 22) {
                ?>
                <a class="btn btn-primary" href="<?php echo publicUrl('create-player') ?>">Add New Player</a>
                <?php
            }
            ?>
            <a class="btn btn-danger prompt-team-delete" href="<?php echo publicUrl('delete-team') ?>">Delete
                team</a>
        </div>
        <?php
    } else {
        ?>
        <div class="alert alert-warning">
            You don't have any player in your team "<?php echo $team->name ?>". You can create player <a
                    href="<?php echo publicUrl('create-player') ?>">here</a>.
        </div>
        <div class="pull-right">
            <a class="btn btn-danger prompt-team-delete" href="<?php echo publicUrl('delete-team') ?>">Delete
                team</a>
        </div>
        <?php
    }
} else {
    ?>
    <div class="alert alert-warning">
        You don't have a team yet! Please create a team <a href="<?php echo publicUrl('create-team') ?>">here</a>.
    </div>
    <?php
}
?>
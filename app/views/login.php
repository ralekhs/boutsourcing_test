<?php
// display flash messages
if (getData('errors')) {
    echo printErrors(getData('errors'));
}

if (getData('error')) {
    echo printErrors(getData('error'));
}

if (getData('success')) {
    echo printSuccess(getData('success'));
}
?>
<form class="form-vertical" method="post" action="<?php echo publicUrl('login') ?>">
    <div class="form-group">
        <label>Email:</label>
        <input class="form-control" type="text" name="email" value="<?php echo isset($oldData) ? $oldData['email'] : '' ?>">
    </div>
    <div class="form-group">
        <label>Password:</label>
        <input class="form-control" type="password" name="password" value="<?php echo isset($oldData) ? $oldData['password'] : '' ?>">
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" value="Login">
    </div>
</form>
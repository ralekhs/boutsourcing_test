<?php
if (getData('errors')) {
    echo printErrors(getData('errors'));

    // input that's been sent to the route
    $oldData = getData('old_input');
}
?>

<h1 class="home-title">Create your team coach</h1>

<form class="form-vertical" method="post" action="<?php echo publicUrl('create-team') ?>">
    <div class="form-group">
        <label>Name:</label>
        <input class="form-control" type="text" name="name"
               value="<?php echo isset($oldData) ? $oldData['name'] : '' ?>">
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" value="Save">
    </div>
</form>

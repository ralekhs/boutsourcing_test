<?php
if (getData('errors')) {
    echo printErrors(getData('errors'));

    // input that's been sent to the route
    $oldData = getData('old_input');
}
?>

<h1 class="home-title">Add new player coach</h1>

<form class="form-vertical" method="post" action="<?php echo publicUrl('create-player') ?>">
    <div class="form-group">
        <label>Name:</label>
        <input class="form-control" type="text" name="name"
               value="<?php echo isset($oldData) ? $oldData['name'] : '' ?>">
    </div>
    <div class="form-group">
        <label>Position:</label>
        <select class="form-control" name="position">
            <?php
            foreach (getData('positions') as $position) {
                $selected = isset($oldData) && $oldData['position'] == $position ? 'selected' : '';
                echo "<option value='{$position}' {$selected}>{$position}</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label>Quality:</label>
        <input class="form-control" type="number" name="quality" min="1" max="5"
               value="<?php echo isset($oldData) ? $oldData['quality'] : '' ?>">
    </div>
    <div class="form-group">
        <label>Speed:</label>
        <input class="form-control" type="number" name="speed" min="1" max="5"
               value="<?php echo isset($oldData) ? $oldData['speed'] : '' ?>">
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" value="Save">
    </div>
</form>

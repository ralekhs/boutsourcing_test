<?php
$players = getData('players');

// display flash messages
if (getData('success')) {
    echo printSuccess(getData('success'));
}
if (getData('error')) {
    echo printErrors(getData('error'));
}
?>
<div class="alert alert-info">
    Due to missing players you have automatically lost this match. Please complete match (click on a button).
</div>
<div>
    <form action="<?php echo publicUrl('complete-match')?>" method="post">
        <input type="hidden" name="ids" value="<?php echo getData('playerIds')?>">
        <button class="btn btn-primary" type="submit">Complete match</button>
    </form>
</div>

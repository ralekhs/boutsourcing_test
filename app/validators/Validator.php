<?php

namespace App\Validators;

use App\Models\Player;

/**
 * Validator contains methods that handle validation of inputs
 * Realized as singleton, to make sure we always get the same instance.
 */
class Validator
{
    const max_goalies = 2,
        max_defenders = 6,
        max_midfielders = 10,
        max_strikers = 4;

    private static $validator;

    private function __construct(){}

    public static function instance()
    {
        if (is_null(self::$validator)) {
            self::$validator = new self();
        }
        return self::$validator;
    }

    /**
     * Validate registration inputs and return array with possible errors.
     *
     * @param array $data
     * @return array
     */
    public function validateRegistration($data)
    {
        $errors = [];

        if (array_key_exists('email', $data)) {
            $email = $data['email'];
            if ($email != '') {
                if (!strpos($email, '@')) {
                    $errors[] = 'Email is not valid.';
                }
            } else {
                $errors[] = 'Email can\'t be empty value.';
            }
        } else {
            $errors[] = 'Email is required.';
        }

        if (array_key_exists('password', $data)) {
            if (array_key_exists('password_repeat', $data)) {
                $password = $data['password'];
                $passwordRepeat = $data['password_repeat'];
                if ($password != '') {
                    if ($passwordRepeat != '') {
                        if ($password != $passwordRepeat) {
                            $errors[] = 'Password repeat must be same as password.';
                        }
                    } else {
                        $errors[] = 'Password repeat can\'t be empty value.';
                    }
                } else {
                    $errors[] = 'Password can\'t be empty value.';
                    if ($passwordRepeat == '') {
                        $errors[] = 'Password repeat can\'t be empty value.';
                    }
                }
            } else {
                $errors[] = 'Password repeat is required.';
            }
        } else {
            $errors[] = 'Password is required.';
            if (!array_key_exists('password_repeat', $data)) {
                $errors[] = 'Password repeat is required.';
            }
        }

        return $errors;
    }

    /**
     * Validate login inputs and return array with possible errors.
     *
     * @param array $data
     * @return array
     */
    public function validateLogin($data)
    {
        $errors = [];
        if (array_key_exists('email', $data)) {
            $email = $data['email'];
            if ($email != '') {
                if (!strpos($email, '@')) {
                    $errors[] = 'Email is not valid.';
                }
            } else {
                $errors[] = 'Email can\'t be empty value.';
            }
        } else {
            $errors[] = 'Email is required.';
        }

        if (array_key_exists('password', $data)) {
            $password = $data['password'];
            if ($password == '') {
                $errors[] = 'Password can\'t be empty value.';
            }
        } else {
            $errors[] = 'Password is required.';
        }

        return $errors;
    }

    /**
     * Validate team creation inputs and return array with possible errors.
     *
     * @param array $data
     * @return array
     */
    public function validateTeam($data)
    {
        $errors = [];
        if (array_key_exists('name', $data)) {
            $name = $data['name'];
            if ($name == '') {
                $errors[] = 'Name can\'t be empty value.';
            }
        } else {
            $errors[] = 'Name is required.';
        }

        return $errors;
    }

    /**
     * Validate player creation inputs and return array with possible errors.
     *
     * @param array $data
     * @return array
     */
    public function validatePlayer($data)
    {
        $errors = [];
        if (array_key_exists('name', $data)) {
            $name = $data['name'];
            if ($name == '') {
                $errors[] = 'Name can\'t be empty value.';
            }
        } else {
            $errors[] = 'Name is required.';
        }

        if (array_key_exists('position', $data)) {
            $position = $data['position'];
            if ($position == '' || !in_array($position, Player::positions)) {
                $errors[] = 'Position must be value from: ' . implode(', ', Player::positions);
            }

            // check if max number of players for particular position isn't passed
            $positionCount = Player::select(['count(*) as position_count'], [
                ['team_id', '=', auth()->team()->id],
                ['position', '=', $position]
            ])['position_count'];

            switch ($position) {
                case 'Goalie':
                    if ($positionCount >= self::max_goalies) {
                        $errors[] = 'You already have max number of goalies.';
                    }
                    break;
                case 'Defender':
                    if ($positionCount >= self::max_defenders) {
                        $errors[] = 'You already have max number of defenders.';
                    }
                    break;
                case 'Midfielder':
                    if ($positionCount >= self::max_midfielders) {
                        $errors[] = 'You already have max number of midfielders.';
                    }
                    break;
                case 'Striker':
                    if ($positionCount >= self::max_strikers) {
                        $errors[] = 'You already have max number of strikers.';
                    }
                    break;
                default:
                    break;
            }
        } else {
            $errors[] = 'Position is required.';
        }

        if (array_key_exists('quality', $data)) {
            $quality = $data['quality'];
            if ($quality == '' || $quality > 5 || $quality < 1) {
                $errors[] = 'Quality must be between 1-5.';
            }
        } else {
            $errors[] = 'Quality is required.';
        }

        if (array_key_exists('speed', $data)) {
            $speed = $data['speed'];
            if ($speed == '' || $speed > 5 || $speed < 1) {
                $errors[] = 'Speed must be between 1-5.';
            }
        } else {
            $errors[] = 'Speed is required.';
        }

        return $errors;
    }

}

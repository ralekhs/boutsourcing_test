<?php

namespace App\Models;

use App\Core\Database\Model;

class Player extends Model
{
    const positions = [
        'Goalie', 'Defender', 'Midfielder', 'Striker'
    ];

    protected $tableName = 'players';

    public function team()
    {
        return Team::find($this->team_id);
    }
}
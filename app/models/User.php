<?php

namespace App\Models;

use App\Core\Database\Model;

class User extends Model
{
    protected $tableName = 'users';

    /*
     * Return team that belongs to this coach
     */
    public function team()
    {
        return Team::find([
            [
                'coach_id',
                '=',
                $this->id
            ]
        ]);
    }
}
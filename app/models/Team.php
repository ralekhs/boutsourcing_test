<?php

namespace App\Models;

use App\Core\Database\Model;

class Team extends Model
{
    protected $tableName = 'teams';

    /*
     * Returns coach of this team
     */
    public function coach()
    {
        return User::find($this->coach_id);
    }

    /*
     * Returns roster of this team
     */
    public function players($injured = null)
    {
        $conditions = [
            ['team_id', '=', $this->id]
        ];

        if (is_bool($injured)) {
            $conditions[] = ['injured', '=', (int) $injured];
        }

        return Player::findAll($conditions, ['position']);
    }
}
<?php

namespace App\Core\Database;

/**
 * Class used for calling DB methods, realized as Facade
 */
class DBFacade
{
    public static function select($tableName, $primaryKey, array $select = null, array $where = null, array $orderBy = null, $limit = null)
    {
        return DB::instance()->select($tableName, $primaryKey, $select, $where, $orderBy, $limit);
    }

    public static function selectRaw($sqlQuery, $class = null)
    {
        return DB::instance()->selectRaw($sqlQuery, $class);
    }

    public static function save($tableName, $primaryKey, $attributes)
    {
        return DB::instance()->save($tableName, $primaryKey, $attributes);
    }

    public static function delete($tableName, $primaryKey, $value)
    {
        return DB::instance()->delete($tableName, $primaryKey, $value);
    }
}
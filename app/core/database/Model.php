<?php

namespace App\Core\Database;

use App\Core\Database\DBFacade as DB;

/**
 * Abstract class Model defines our models, and contain functions that all models should have (implement).
 * Its primarily used for db operations over certain model, etc.
 */
abstract class Model implements ModelInterface
{
    protected $tableName = null;
    protected $primaryKey = 'id';

    /**
     * Find single row from database and return wanted object
     * @param array $data data provided for query
     * @return Object|null
     */
    public static function find($data)
    {
        $className = get_called_class();
        $object = new $className;
        $where = $data;

        if (is_numeric($data)) {
            $where = [
                [
                    $object->primaryKey,
                    '=',
                    $data
                ]
            ];
        }

        $results = DB::select($object->tableName, $object->primaryKey, null, $where, null, 1);

        if (is_null($results) || empty($results)) {
            return null;
        }

        foreach (get_object_vars($results[0]) as $key => $data) {
            $object->$key = $data;
        }

        return $object;

    }

    /**
     * Find all rows from database and return Object collection as array
     * @param $data params provided for query
     * @param array|null $orderBy
     * @return array|null
     */
    public static function findAll($data, array $orderBy = null)
    {
        $className = get_called_class();
        $object = new $className;

        $results = DB::select($object->tableName, $object->primaryKey, null, $data, $orderBy, null);

        if (is_null($results)) {
            return null;
        }

        $resultSet = [];

        foreach ($results as $object) {
            $resultObject = new $className;
            foreach (get_object_vars($object) as $key => $data) {
                $resultObject->$key = $data;
            }
            $resultSet[] = $resultObject;
        }

        return $resultSet;
    }

    /**
     * Custom query for db records
     * @param array $select
     * @param array|null $where
     * @param array|null $order
     * @param null $limit
     * @return array|null
     */
    public static function select(array $select, array $where = null, array $order = null, $limit = null)
    {
        $className = get_called_class();
        $object = new $className;

        $results = DB::select($object->tableName, $object->primaryKey, $select, $where, $order, $limit);

        if (is_null($results)) {
            return null;
        }

        if (count($results) < 2) {
            return get_object_vars($results[0]);
        }

        $resultSet = [];

        foreach ($results as $object) {
            $resultSet[] = get_object_vars($object);
        }

        return $resultSet;
    }

    /**
     * Saving models, new or updated ones
     * @return bool
     */
    public function save()
    {
        $attributes = array_diff_key(get_object_vars($this), get_class_vars(self::class));
        return DB::save($this->tableName, $this->primaryKey, $attributes);
    }

    /**
     * Delete certain model
     * @return bool
     */
    public function delete()
    {
        $className = get_called_class();

        $primaryKeyString = ($this->primaryKey);
        $value = $this->$primaryKeyString;

        $result = DB::delete($this->tableName, $this->primaryKey, $value);

        // if deleting User model, logout that user
        if ($result && $className == 'App\Models\User') {
            userLogout();
        }

        return $result;
    }
}
<?php

namespace App\Core\Router;

use Exception;

/**
 * Router works with our routes, holds them in a collection,
 * and contains process() method that gives us some response.
 * 
 * @property array $routeList List of all our routes
 */
class Router {

    private $routeList = [];

    /**
     * Add route with method GET to the $routeList
     * 
     * @param string $uri
     * @param string $function
     * @param boolean $authNeeded
     */
    public function get($uri, $function, $authNeeded = false) {
        $route = new Route();
        $route->setUri($uri);
        $route->setMethod('GET');
        $route->setFunction($function);
        $route->setAuthNeeded($authNeeded);
        $this->routeList[] = $route;
    }

    /**
     * Add route with method POST to the $routeList
     * 
     * @param string $uri
     * @param string $function
     * @param boolean $authNeeded
     */
    public function post($uri, $function, $authNeeded = false) {
        $route = new Route();
        $route->setUri($uri);
        $route->setMethod('POST');
        $route->setFunction($function);
        $route->setAuthNeeded($authNeeded);
        $this->routeList[] = $route;
    }

    /**
     * Method that is executed on the start of our app.
     * It returns a view or redirects to a route,
     * 
     * @return mixed
     * @throws Exception
     */
    public function process() {
        // if uri is empty set it to /
        $uri = isset($_REQUEST['uri']) ? '/' . $_REQUEST['uri'] : '/';

        // instance of class that will be used for callback, in our case controller
        $functionClassObject = null;
        
        // name of the class that will be used for callback
        $functionClassString = null;
        
        // method in class that will be user for callback
        $function = null;

        try {
            
            // search in routeList the requested route
            foreach ($this->routeList as $route) {
                
                // if route exists
                if ($route->getUri() == $uri) {
                    
                    // checking method, post or get
                    if ($_SERVER['REQUEST_METHOD'] == $route->getMethod()) {

                        if ($route->getAuthNeeded() && !checkAuth()) {
                            redirect('login');
                        }
                        
                        // separate class and method
                        $functionArray = explode("@", $route->getFunction());
                        
                        // there must be 2 indexes, first for the class and second for the method
                        if (count($functionArray) == 2) {
                            $functionClassString = 'App\Controllers\\' . $functionArray[0];
                            $function = $functionArray[1];
                            
                            // check if class exists, in our case controller
                            if (class_exists($functionClassString)) {
                                
                                // instance our class into variable above
                                $functionClassObject = new $functionClassString;
                                
                                // check if method exists inside our class
                                if (method_exists($functionClassObject, $function)) {
                                    
                                    // if exists, we will get some callback, and we dont need uri anymore
                                    unset($_REQUEST['uri']);
                                    
                                    // do something
                                    return $functionClassObject->$function();
                                }
                                
                                // if function doesn't exist
                                throw new Exception('Missing method: ' . $function . ' in class "' . $functionClassString . '".');
                            }
                            
                            // if class doesn't exist
                            throw new Exception('Missing class: "' . $functionClassString . '".');
                        }
                        
                        // if function is in bad format
                        throw new Exception("Callback function string must be in a format: class_name@method_name");
                    }
                }
            }
            
            // if route doesn't exist
            throw new Exception('Route doesn\'t exist.');
        } catch (Exception $exc) {
            
            // handle exception
            echo "<b>Exception occured:</b> ({$exc->getFile()} | Line: {$exc->getLine()})<br>";
            echo $exc->getMessage();
            echo "<br>";
            echo implode('<br>#', explode('#', $exc->getTraceAsString()));
        }
    }

}

<?php

/*
 * This file holds most of the sneaky functions of our app.
 */

function config($search)
{
    $configArray = parse_ini_file('config.ini');
    return $configArray[$search];
}

/*
 * Returns desired URL or just a public folder path.
 */
function publicUrl($route = '')
{
    return config('public_url') . $route;
}

/*
 * Render a main view.
 * Method accepts view name and optional variables that will be avaliable.
 * to the view.
 */
function view($view, array $data = [])
{
    $_SESSION['view'] = $view;
    if (count($data)) {
        foreach ($data as $key => $value) {
            $_SESSION[$key] = $value;
        }
    }
    return include __DIR__ . '/app/views/layout.php';
}

/*
 * Renders particular view in some of main views.
 */
function getView($view)
{
    if (file_exists(__DIR__ . '/app/views/' . $view . '.php')) {
        include __DIR__ . '/app/views/' . $view . '.php';
    } else {
        throw new Exception('View "' . $view . '" doesn\'t exist.');
    }
}

/*
 * This method gives us a data that's avaliable in views.
 */
function getData($name)
{
    if (isset($_SESSION[$name])) {
        return $_SESSION[$name];
    }
    return null;
}

/*
 * Rediret method redirect us to given URL.
 * It also accepts variables that will be avaliable on that URL.
 */
function redirect($to, $flashData = [])
{
    $redirectUrl = publicUrl($to);
    if (is_numeric(strpos($to, 'http'))) {
        $redirectUrl = $to;
    }
    if (count($flashData)) {
        foreach ($flashData as $key => $value) {
            $_SESSION[$key] = $value;
        }
    }
    $headerString = 'Location: ' . $redirectUrl;
    header($headerString);
    exit();
}

/*
 * Get all or particular data from request.
 */
function input($key = null)
{
    if ($key) {
        if (array_key_exists($key, $_REQUEST)) {
            return $_REQUEST[$key];
        }
        return null;
    }
    return $_REQUEST;
}

/*
 * Display errors on the view.
 * It can be array of errors or a single error.
 */
function printErrors($errors)
{
    if (is_array($errors)) {
        $errorsString = '<div class="alert alert-danger"><ul>';
        foreach ($errors as $error) {
            $errorsString .= '<li>' . $error . '</li>';
        }
        $errorsString .= '</ul></div>';
        return $errorsString;
    }

    $errorString = '<div class="alert alert-danger"><p>';
    $errorString .= $errors;
    $errorString .= '</p></div>';
    return $errorString;
}

/*
 * Display success message on the view.
 */
function printSuccess($text)
{
    $successString = '<div class="alert alert-success"><p>';
    $successString .= $text;
    $successString .= '</p></div>';
    return $successString;
}

/*
 * Login user.
 */
function userLogin($user)
{
    $_SESSION['id'] = $user->id;
    $_SESSION['name'] = $user->email;
}

/*
 * Logout user.
 */
function userLogout()
{
    session_unset();
    session_destroy();
}

/*
 * Check if user is logged in.
 */
function checkAuth()
{
    if (getData('id')) {
        return true;
    }
    return false;
}

/*
 * Remove all flash data except user auth data.
 */
function removeFlashData()
{
    unset($_SESSION['success']);
    unset($_SESSION['error']);
    unset($_SESSION['errors']);
    unset($_SESSION['sunglasses']);
    unset($_SESSION['term']);
}

/*
 * Return authenticated user
 */
function auth()
{
    if (checkAuth()) {
        return \App\Models\User::find(getData('id'));
    }

    return null;
}

/*
 * Dump helper
 */
function dd($data)
{
    die(var_dump($data));
}